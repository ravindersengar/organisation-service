package com.xlrs.organisation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xlrs.organisation.entity.LegalEntity;
import com.xlrs.organisation.entity.Organisation;

@Repository
public interface LegalEntityRepository extends JpaRepository<LegalEntity, Long>{

	@Query("SELECT le FROM LegalEntity le WHERE le.organisation = ?1")
	List<LegalEntity> getByOrganisationId(Organisation org);

}
