package com.xlrs.organisation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xlrs.organisation.entity.OrganisationSystem;

@Repository
public interface OrganisationSystemRepository extends JpaRepository<OrganisationSystem, Long>{

}
