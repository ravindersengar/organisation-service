package com.xlrs.organisation.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.xlrs.organisation.entity.System;

@Repository
public interface SystemRepository extends JpaRepository<System, Long>{

}
