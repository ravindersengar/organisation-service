package com.xlrs.organisation.view;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.view.BaseView;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class OrgaisationSystemView implements BaseView{

	private static final long serialVersionUID = 1L;

	private Long id;
	
	@NotNull(message = "{orgsystem.start.date.mandatory.feild.notempty}")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date startDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date endDate;
	
	@NotEmpty(message = "{orgsystem.name.mandatory.feild.notempty}")
	private String name;
	
	private Long organisationId;
	
	private Long systemId;
}
