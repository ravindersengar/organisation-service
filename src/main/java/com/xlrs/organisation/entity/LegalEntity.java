package com.xlrs.organisation.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(Include.NON_NULL)
public class LegalEntity extends AbstractEntity{

	private static final long serialVersionUID = 1779829476650834122L;
	
	
	@ManyToOne
	@JoinColumn(name="organisation_id")
	@JsonBackReference
	private Organisation organisation;
	
	@NotEmpty(message = "{legal.entity.name.mandatory.feild.notempty}")
	private String name;

}
