package com.xlrs.organisation.entity;

import javax.persistence.Entity;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class System extends AbstractEntity{

	private static final long serialVersionUID = -4511545954841540563L;
	@NotEmpty(message = "{system.name.mandatory.feild.notempty}")
	private String name;
	@NotEmpty(message = "{system.type.mandatory.feild.notempty}")
	private String type;
	@NotEmpty(message = "{system.code.mandatory.feild.notempty}")
	private String code;
	
}
