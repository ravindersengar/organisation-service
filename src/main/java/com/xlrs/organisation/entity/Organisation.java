package com.xlrs.organisation.entity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = false)
@JsonInclude(Include.NON_NULL)
public class Organisation extends AbstractEntity {

	private static final long serialVersionUID = 3304966215540493348L;

	@NotEmpty(message = "{organisation.name.mandatory.feild.notempty}")
	private String name;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "organisation")
	@JsonManagedReference
	private List<LegalEntity> legalEntities;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "organisation")
	@JsonManagedReference
	private List<OrganisationSystem> organizationSystems;

	public OrganisationSystem getOrganizationSystem(Long id) {
		return Optional.ofNullable(this.organizationSystems).orElse(Collections.emptyList()).stream()
				.filter(os -> os.getId().equals(id) && os.getStatus().equals(this.getStatus()))
				.collect(Collectors.toList()).get(0);
	}

}
