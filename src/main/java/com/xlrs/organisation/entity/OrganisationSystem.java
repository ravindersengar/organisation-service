package com.xlrs.organisation.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.xlrs.commons.entity.AbstractEntity;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper=false)
@JsonInclude(Include.NON_NULL)
public class OrganisationSystem extends AbstractEntity{
	
	private static final long serialVersionUID = 5107978604979177730L;
	
	@NotNull(message = "{orgsystem.start.date.mandatory.feild.notempty}")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date startDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date endDate;
	
	@NotEmpty(message = "{orgsystem.name.mandatory.feild.notempty}")
	private String name;
	
	@ManyToOne 
	@JoinColumn(name="organisation_id")
	@JsonBackReference
	private Organisation organisation;
	
	@ManyToOne
	@JoinColumn(name="system_id")
	private System system;

}
