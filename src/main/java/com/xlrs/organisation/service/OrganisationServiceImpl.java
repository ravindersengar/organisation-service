package com.xlrs.organisation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.organisation.entity.Organisation;
import com.xlrs.organisation.repository.OrganisationRepository;
import com.xlrs.organisation.view.OrganisationView;

@Service
public class OrganisationServiceImpl implements OrganisationService {

	@Autowired
	private OrganisationRepository organisationRepository;
	
	@Autowired
	private BaseRepository<Organisation> baseRepository;
	
	@Autowired
	private MessageSource messages;

	@Override
	public OrganisationView createOrganisation(OrganisationView createOrganisationView) throws NoResultFoundException {
		Organisation organisation = createOrganisationView.getId()!=null ? getOrganisationById(createOrganisationView.getId()) : new Organisation() ;
		organisation.setName(createOrganisationView.getName());
		organisation = organisationRepository.save(baseRepository.addAuditFeilds(organisation, null, CommonConstants.STATUS_ACTIVE));
		return new OrganisationView(organisation.getName(), organisation.getId());
	}

	@Override
	public OrganisationView getOrganisation(Long id) throws NoResultFoundException {
		Organisation org = getOrganisationById(id);
		return new OrganisationView(org.getName(), org.getId());
	}

	public Organisation getOrganisationById(Long id) throws NoResultFoundException {
		Optional<Organisation> optional = organisationRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		Organisation org = optional.get();
		return org;
	}

}
