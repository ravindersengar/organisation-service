package com.xlrs.organisation.service;

import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.organisation.entity.Organisation;
import com.xlrs.organisation.view.OrganisationView;

public interface OrganisationService {

	public OrganisationView createOrganisation(OrganisationView createOrganisationView) 
			throws NoResultFoundException;

	public OrganisationView getOrganisation(Long id) throws  NoResultFoundException;
	
	public Organisation getOrganisationById(Long id) throws NoResultFoundException;

	
}
