package com.xlrs.organisation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.organisation.entity.OrganisationSystem;
import com.xlrs.organisation.entity.System;
import com.xlrs.organisation.repository.OrganisationSystemRepository;
import com.xlrs.organisation.view.OrgaisationSystemView;
import com.xlrs.organisation.view.SystemView;

@Service
public class OrganisationSystemServiceImpl implements OrganisationSystemService{

	@Autowired
	private OrganisationSystemRepository organisationSystemRepository;
	
	@Autowired
	private MessageSource messages;
	
	@Autowired
	private BaseRepository<OrganisationSystem> baseRepository;
	
	@Autowired
	private SystemService systemService;
	
	@Autowired
	private OrganisationService organisationService;
		
	
	@Override
	public OrgaisationSystemView createOrganisationSystem(OrgaisationSystemView orgaisationSystemView) throws NoResultFoundException {
		OrganisationSystem organisationSystem = orgaisationSystemView.getId()!=null ? getOrganisationSystemById(orgaisationSystemView.getId()) : new OrganisationSystem() ;
		organisationSystem.setName(orgaisationSystemView.getName());
		organisationSystem.setStartDate(orgaisationSystemView.getStartDate());
		organisationSystem.setEndDate(orgaisationSystemView.getEndDate());
		organisationSystem.setOrganisation(organisationService.getOrganisationById(orgaisationSystemView.getOrganisationId()));
		organisationSystem.setSystem(systemService.getSystemById(orgaisationSystemView.getSystemId()));
		organisationSystem = organisationSystemRepository.save(baseRepository.addAuditFeilds(organisationSystem, null, CommonConstants.STATUS_ACTIVE));
		return new OrgaisationSystemView(organisationSystem.getId(), organisationSystem.getStartDate(), organisationSystem.getEndDate(), organisationSystem.getName(), organisationSystem.getOrganisation().getId(), organisationSystem.getSystem().getId());
	}

	@Override
	public OrgaisationSystemView getOrganisationSystem(Long id) throws NoResultFoundException {
		OrganisationSystem organisationSystem = getOrganisationSystemById(id);
		return new OrgaisationSystemView(organisationSystem.getId(), organisationSystem.getStartDate(), organisationSystem.getEndDate(), organisationSystem.getName(), organisationSystem.getOrganisation().getId(), organisationSystem.getSystem().getId());
	}

	private OrganisationSystem getOrganisationSystemById(Long id) throws NoResultFoundException {
		Optional<OrganisationSystem> optional = organisationSystemRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		OrganisationSystem organisationSystem = optional.get();
		return organisationSystem;
	}

	@Override
	public SystemView getSystemByOrganisationSystemId(Long orgSystemId) throws NoResultFoundException {
		Optional<OrganisationSystem> optional = organisationSystemRepository.findById(orgSystemId);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		System system = optional.get().getSystem();
		return new SystemView(system.getId(), system.getName(), system.getType(), system.getCode());
	}

}
