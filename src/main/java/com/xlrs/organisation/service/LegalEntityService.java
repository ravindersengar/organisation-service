package com.xlrs.organisation.service;

import java.util.List;

import org.springframework.context.NoSuchMessageException;

import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.organisation.view.LegalEntityView;

public interface LegalEntityService {

	LegalEntityView createLegalEntity(LegalEntityView legalEntityView) throws NoResultFoundException;

	LegalEntityView getLegalEntity(Long id) throws NoResultFoundException;

	List<Long> getLegalEntityIdsByOrgId(Long orgId) throws NoSuchMessageException, NoResultFoundException;

}
