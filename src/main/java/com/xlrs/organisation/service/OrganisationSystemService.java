package com.xlrs.organisation.service;

import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.organisation.view.OrgaisationSystemView;
import com.xlrs.organisation.view.SystemView;

public interface OrganisationSystemService {

	OrgaisationSystemView createOrganisationSystem(OrgaisationSystemView systemView) throws NoResultFoundException;

	OrgaisationSystemView getOrganisationSystem(Long id) throws NoResultFoundException;

	SystemView getSystemByOrganisationSystemId(Long orgSystemId) throws NoResultFoundException;

}
