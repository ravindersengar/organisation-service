package com.xlrs.organisation.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.stereotype.Service;

import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.organisation.entity.LegalEntity;
import com.xlrs.organisation.entity.Organisation;
import com.xlrs.organisation.repository.LegalEntityRepository;
import com.xlrs.organisation.view.LegalEntityView;

@Service
public class LegalEntityServiceImpl implements LegalEntityService{

	@Autowired
	private LegalEntityRepository legalEntityRepository; 
	
	@Autowired
	private OrganisationService organisationService;
	
	@Autowired
	private BaseRepository<LegalEntity> baseRepository;
	
	@Autowired
	private MessageSource messages;
	
	@Override
	public LegalEntityView createLegalEntity(LegalEntityView legalEntityView) throws NoResultFoundException {
		LegalEntity legalEntity = legalEntityView.getId()!=null ? getLegalEntityById(legalEntityView.getId()) : new LegalEntity() ;
		legalEntity.setName(legalEntityView.getName());
		legalEntity.setOrganisation(organisationService.getOrganisationById(legalEntityView.getOrganisationId()));
		legalEntity = legalEntityRepository.save(baseRepository.addAuditFeilds(legalEntity, null, CommonConstants.STATUS_ACTIVE));
		Organisation organisation = legalEntity.getOrganisation();
		return new LegalEntityView(legalEntity.getName(),organisation.getId(), organisation.getName(), legalEntity.getId());
	}

	@Override
	public LegalEntityView getLegalEntity(Long id) throws NoResultFoundException {
		LegalEntity legalEntity = getLegalEntityById(id);
		Organisation organisation = legalEntity.getOrganisation();
		return new LegalEntityView(legalEntity.getName(),organisation.getId(), organisation.getName(), legalEntity.getId());
	}

	private LegalEntity getLegalEntityById(Long id) throws NoResultFoundException {
		Optional<LegalEntity> optional = legalEntityRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		LegalEntity legalEntity = optional.get();
		return legalEntity;
	}

	@Override
	public List<Long> getLegalEntityIdsByOrgId(Long orgId) throws NoSuchMessageException, NoResultFoundException {
		List<Long> entityIds = new ArrayList<Long>();
		Organisation org = organisationService.getOrganisationById(orgId);
		if(org == null) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		List<LegalEntity> legalEntities = legalEntityRepository.getByOrganisationId(org);
		if(legalEntities== null || legalEntities.isEmpty()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		for (LegalEntity le : legalEntities) {
			entityIds.add(le.getId());
		}
		return entityIds;
	}


}
