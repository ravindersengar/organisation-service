package com.xlrs.organisation.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.xlrs.commons.constant.CommonConstants;
import com.xlrs.commons.entity.BaseRepository;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.organisation.entity.System;
import com.xlrs.organisation.repository.SystemRepository;
import com.xlrs.organisation.view.SystemView;

@Service
public class SystemServiceImpl implements SystemService{

	@Autowired
	private SystemRepository systemRepository;

	@Autowired
	private BaseRepository<System> baseRepository;
	
	@Autowired
	private MessageSource messages;
	
	@Override
	public SystemView createSystem(SystemView systemView) throws NoResultFoundException {
		System system = systemView.getId()!=null ? getSystemById(systemView.getId()) : new System() ;
		system.setCode(systemView.getCode());
		system.setName(systemView.getName());
		system.setType(systemView.getType());
		system = systemRepository.save(baseRepository.addAuditFeilds(system, null, CommonConstants.STATUS_ACTIVE));
		return new SystemView(system.getId(), system.getName(), system.getType(), system.getCode());
	}

	@Override
	public SystemView getSystem(Long id) throws NoResultFoundException {
		System system = getSystemById(id);
		return new SystemView(system.getId(), system.getName(), system.getType(), system.getCode());
	}

	public System getSystemById(Long id) throws NoResultFoundException {
		Optional<System> optional = systemRepository.findById(id);
		if(!optional.isPresent()) {
			throw new NoResultFoundException(messages.getMessage("err.result.not.found", null, null));
		}
		System system = optional.get();
		return system;
	}

}
