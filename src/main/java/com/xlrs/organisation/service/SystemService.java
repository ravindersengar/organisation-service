package com.xlrs.organisation.service;

import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.organisation.entity.System;
import com.xlrs.organisation.view.SystemView;

public interface SystemService {

	SystemView createSystem(SystemView systemView) throws NoResultFoundException;

	SystemView getSystem(Long id) throws NoResultFoundException;
	
	System getSystemById(Long id) throws NoResultFoundException;

}
