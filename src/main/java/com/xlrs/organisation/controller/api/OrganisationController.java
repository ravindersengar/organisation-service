package com.xlrs.organisation.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.organisation.service.OrganisationService;
import com.xlrs.organisation.view.OrganisationView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/organisation")
@Slf4j
public class OrganisationController {

	@Autowired
	private OrganisationService organisationService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createOrganisation(@RequestBody OrganisationView createOrganisationView) throws Exception {
		log.debug("Requested payload is : " + createOrganisationView);
		try {
			return new ResponseView(organisationService.createOrganisation(createOrganisationView));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseView getOrganisation(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(organisationService.getOrganisation(id));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/health")
	public String health() throws Exception {
		return "success";
	}
}
