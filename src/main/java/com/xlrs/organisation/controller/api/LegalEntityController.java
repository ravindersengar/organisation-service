package com.xlrs.organisation.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.organisation.service.LegalEntityService;
import com.xlrs.organisation.view.LegalEntityView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/organisation/legalentity")
@Slf4j
public class LegalEntityController  {

	@Autowired
	private LegalEntityService legalEntityService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createLegalEntity(@RequestBody LegalEntityView legalEntityView) throws Exception {
		log.debug("Requested payload is : " + legalEntityView);
		
		try {
			return new ResponseView(legalEntityService.createLegalEntity(legalEntityView));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseView getLegalEntity(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(legalEntityService.getLegalEntity(id));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{orgId}/legalentities")
	public ResponseView getLegalEntityIdsByOrgId(@PathVariable Long orgId) throws Exception {
		return new ResponseView(legalEntityService.getLegalEntityIdsByOrgId(orgId));
	}

}
