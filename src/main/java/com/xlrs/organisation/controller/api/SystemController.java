package com.xlrs.organisation.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.organisation.service.OrganisationSystemService;
import com.xlrs.organisation.service.SystemService;
import com.xlrs.organisation.view.SystemView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/organisation/system")
@Slf4j
public class SystemController {

	@Autowired
	private SystemService systemService;
	
	@Autowired
	private OrganisationSystemService organisationSystemService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createSystem(@RequestBody SystemView systemView) throws Exception {
		log.debug("Requested payload is : " + systemView);
		
		try {
			return new ResponseView(systemService.createSystem(systemView));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseView getSystem(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(systemService.getSystem(id));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{orgSystemId}/system")
	public ResponseView getSystemByOrgSystemId(@PathVariable Long orgSystemId) throws Exception {
		try {
			SystemView systemView = organisationSystemService.getSystemByOrganisationSystemId(orgSystemId);
			return new ResponseView(systemView);
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
}
