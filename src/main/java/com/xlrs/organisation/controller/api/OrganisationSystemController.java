package com.xlrs.organisation.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.xlrs.commons.exception.ApplicationException;
import com.xlrs.commons.exception.NoResultFoundException;
import com.xlrs.commons.view.ResponseView;
import com.xlrs.organisation.service.OrganisationSystemService;
import com.xlrs.organisation.view.OrgaisationSystemView;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/organisation/orgsystem")
@Slf4j
public class OrganisationSystemController {

	@Autowired
	private OrganisationSystemService organisationSystemService;
	
	@Autowired
	private MessageSource messages;

	@PostMapping("/create")
	public ResponseView createSystem(@RequestBody OrgaisationSystemView orgaisationSystemView) throws Exception {
		log.debug("Requested payload is : " + orgaisationSystemView);
		
		try {
			return new ResponseView(organisationSystemService.createOrganisationSystem(orgaisationSystemView));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
	@GetMapping("/{id}")
	public ResponseView getOrganisationSystem(@PathVariable Long id) throws Exception {
		try {
			return new ResponseView(organisationSystemService.getOrganisationSystem(id));
		} catch (NoResultFoundException e) {
			throw new NoResultFoundException(e.getMessage());
		} catch (Exception e) {
			throw new ApplicationException(messages.getMessage("err.general.error.message", null, null));
		}
	}
	
}
