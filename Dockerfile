FROM openjdk:8-jdk-alpine
EXPOSE 4002
VOLUME /main-app
ADD /target/organisation-service-0.1.0.jar organisation-service-0.1.0.jar
ENTRYPOINT ["java","-jar","organisation-service-0.1.0.jar"]